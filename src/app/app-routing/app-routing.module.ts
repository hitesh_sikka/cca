import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { LandingPageComponent } from '../landing-page/landing-page.component';
import { UploadDataComponent } from '../upload-data/upload-data.component';

const routes: Routes = [
  {path : '',redirectTo: '/home', pathMatch: 'full' },
  {path : 'login', component : LoginComponent },
  {path : 'home', component : LandingPageComponent },
  {path : 'upload', component : UploadDataComponent  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
    // CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
